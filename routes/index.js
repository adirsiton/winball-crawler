var express = require('express');
var router = express.Router();
const scraper = require('../controllers/scraper');

router.get('/teams', async (req, res, next) => {
  const data = await scraper.getTeams();

  if (data) {
    res.json(data);
  } else {
    res.status(500).send('error while getting teams data');
  }
});

router.get('/games', async (req, res, next) => {
  const data = await scraper.getGames();

  if (data) {
    res.json(data);
  } else {
    res.status(500).send('error while getting games data');
  }
});

router.get('/score/:teamA/:teamB', async (req, res, next) => {
  const data = await scraper.getScore(req.params.teamA, req.params.teamB);

  if(data) {
    res.send(data);
  } else {
    res.status(500).send('error while trying to get game score');
  }
})

module.exports = router;
