const cheerio = require('cheerio');
const got = require('got');

const laLigaTeamsURL = 'https://footystats.org/spain/la-liga';
const laLigaGamesURL = 'https://fbref.com/en/comps/12/schedule/La-Liga-Scores-and-Fixtures';

const getTeams = async () => {
    const response = await got(laLigaTeamsURL,
    );
    const $ = cheerio.load(response.body);
    var data = [];
    $('#league-tables-wrapper > div > div.table-wrapper > table > tbody > tr').each(async (index, element) => {

        const tds = $(element).find("td");
        const pics = $(tds[1]).children().children().children().toArray();
        const names = $(tds[2]).toArray();
        names.map(async (name, index) => {
            data.push({ 'name': $(name).text(), "picPath": $(pics[index]).attr('src') });
        });
    });

    return new Promise((resolve) => {
        return resolve(data);
    })
}

const getGames = async () => {
    const response = await got.get(laLigaGamesURL);
    const $ = cheerio.load(response.body);
    var data = [];
    var currentDate = null;
    $('#sched_ks_10731_1 > tbody > tr').each(async (index, element) => {
        var dataObj = $(element);

        if(dataObj.find('[data-stat=date]').text() !== '') {
            currentDate = new Date(dataObj.find('[data-stat=date]').text());
        }
        
        if (!dataObj.hasClass('spacer') &&
            dataObj.find('[data-stat=notes]').text() !== 'Match Postponed' &&
            !dataObj.hasClass('thead')) {           
            var currGame = {}

            currGame.date = currentDate;
            currGame.time = new Date();
            var time = dataObj.find('[data-stat=time]').text().split(' ')[0];
            currGame.time.setHours(time.split(':')[0]);
            currGame.time.setHours(currGame.time.getHours() + 1);
            currGame.time.setMinutes(time.split(':')[1]);
            currGame.teamA = dataObj.find('[data-stat=squad_a]').text();
            currGame.teamB = dataObj.find('[data-stat=squad_b]').text();
            currGame.score = dataObj.find('[data-stat=score]').text(); // 0:0 or score  if game is started, 'vs' if not started

            data.push(currGame);
        }
    });

    return new Promise((resolve) => {
        return resolve(data);
    });
}

const getScore = async (teamA, teamB) => {
    const response = await got.get(laLigaGamesURL);
    const $ = cheerio.load(response.body);
    var data = '';
    var currentDate = null;
    $('#sched_10731_1 > tbody > tr').each(async (index, element) => {
        var dataObj = $(element);

        if(dataObj.find('[data-stat=date]').text() !== '') {
            currentDate = new Date(dataObj.find('[data-stat=date]').text());
        }
        
        if (!dataObj.hasClass('spacer') &&
            dataObj.find('[data-stat=notes]').text() !== 'Match Postponed' &&
            !dataObj.hasClass('thead')) {           
            var currGame = {}

            const currTeamA = dataObj.find('[data-stat=squad_a]').text();
            const currTeamB = dataObj.find('[data-stat=squad_b]').text();

            if(currTeamA == teamA && currTeamB == teamB) {
                data = dataObj.find('[data-stat=score]').text()
                
            }
        }
    });

    return new Promise((resolve) => {         
        return resolve(data); // 0:0 or score if game is started, 'vs' if not started);
    });
}

exports.getTeams = getTeams;
exports.getGames = getGames;
exports.getScore = getScore;