var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// Uncomment if working from proxy
/*var bootstrap = require('global-agent').bootstrap;
bootstrap();

global.GLOBAL_AGENT.HTTP_PROXY = 'http://10.0.0.10:80';*/

var indexRouter = require('./routes/index');

var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/', indexRouter);

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send(`error: ${err.message}`);
});

module.exports = app;
